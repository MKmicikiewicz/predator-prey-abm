from agents_objects import Prey, Predator, Resources
from pygame.sprite import groupcollide, collide_circle_ratio
from pygame.font import Font
from pygame.math import Vector2


def add_sprites(group, n, **kwargs):
    if kwargs['group_type'] == 'predators':
        for _ in range(n):
            group.add(Predator())
    elif kwargs['group_type'] == 'prey':
        for _ in range(n):
            group.add(Prey())
    elif kwargs['group_type'] == 'resources':
        for _ in range(n):
            group.add(Resources())
    else:
        raise KeyError("Incorrect group type")


def collide(predators, prey, resources):
    collecting_result = groupcollide(prey, resources, False, True)
    for succ_prey in collecting_result.keys():
        if succ_prey.stamina < 10:
            succ_prey.stamina += 1
    hunting_result = groupcollide(predators, prey, False, True)
    for succ_pred in hunting_result.keys():
        succ_pred.stamina = 10

def chase(predators, prey, resources):
    for p in prey:
        position = Vector2(p.rect.center)
        closest_resource = min([r for r in resources], key=lambda r: position.distance_to(Vector2(r.rect.center)))
        p.collect_resources(closest_resource)
        closest_predator = min([pred for pred in predators], key=lambda pred: position.distance_to(Vector2(pred.rect.center)))
        p.flee(closest_predator)

    for predator in predators:
        position = Vector2(predator.rect.center)
        closest_prey = min([p for p in prey], key=lambda p: position.distance_to(Vector2(p.rect.center)))
        predator.chase_prey(closest_prey)



def display_info(predators, prey, resources, screen, font_size, generation=None):
    font = Font(None, font_size)
    pred_surf = font.render(f'Number of predators: {len(predators)}', True, (64, 64, 64))
    pred_rect = pred_surf.get_rect(topleft=(15, 15))

    prey_surf = font.render(f'Number of prey: {len(prey)}', True, (64, 64, 64))
    prey_rect = prey_surf.get_rect(topleft=(15, 35))

    resources_surf = font.render(f'Number of resources: {len(resources)}', True, (64, 64, 64))
    resources_rect = resources_surf.get_rect(topleft=(15, 55))

    screen.blit(pred_surf, pred_rect)
    screen.blit(prey_surf, prey_rect)
    screen.blit(resources_surf, resources_rect)