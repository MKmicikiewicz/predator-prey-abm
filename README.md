# Predator - prey ABM



## Project description
Coevolution between population of predators and population of their prey in a certain
ecosystem describes the changing over time interaction between two species, the
first of which is capable of consuming a significant part of the second's biomass, and 
which can be described using standard mathematical apparatus - Lotka-Volterra equations.

## Goal of the project
The aim of the work is to create a simulation which can model and reproduce
predator-prey co-evolution as faithfully as possible.