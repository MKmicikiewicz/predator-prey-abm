import pygame
from sys import exit
from utility_functions import *


WIDTH = 1440
HEIGHT = 800
FPS = 60
INITIAL_PREDATORS = 1
INITIAL_PREY = 10
INITIAL_RESOURCES = 25

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Predator vs Prey simulation')
clock = pygame.time.Clock()

# spare surface
background_surface = pygame.Surface((WIDTH, HEIGHT))
background_surface.fill('white')
screen.blit(background_surface, (0, 0))

predators = pygame.sprite.Group()
prey = pygame.sprite.Group()
resources = pygame.sprite.Group()

add_sprites(predators, INITIAL_PREDATORS, group_type='predators')
add_sprites(prey, INITIAL_PREY, group_type='prey')
add_sprites(resources, INITIAL_RESOURCES, group_type='resources')

draw_groups = lambda groups: [group.draw(screen) for group in groups]
update_groups = lambda groups: [group.update() for group in groups]

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    screen.blit(background_surface, (0, 0))
    update_groups([prey, predators])
    chase(predators, prey, resources)
    collide(predators, prey, resources)
    draw_groups([prey, predators, resources])
    display_info(predators, prey, resources, screen, 17)
    pygame.display.update()
    clock.tick(FPS)
