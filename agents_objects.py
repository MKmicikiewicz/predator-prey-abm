import numpy as np
import pygame
from pygame.math import Vector2


class Base(pygame.sprite.Sprite):
    def __init__(self, coords=(None, None)):
        super().__init__()
        self.image = pygame.Surface((5, 5))
        self.rect = self.image.get_rect(center=(np.random.randint(0, 1440), np.random.randint(0, 800)) if coords == (None, None) else coords)


class Agent(Base):
    def __init__(self):
        super().__init__()
        self.speed = 5*np.random.random()
        self.stamina = 5
        self.sense =  100
        self.state = 'random'
        self.border = False

    def random_move(self, d_x=None, d_y=None):
        # (1440/800)
        if d_x is None and d_y is None:
            x = np.random.randn()/2.5
            y = np.random.randn()/2.5
            d_x = np.ceil(self.speed*x) if x > 0 else np.floor(self.speed*x)
            d_y = np.ceil(self.speed*y) if y > 0 else np.floor(self.speed*y)

        if (d_x + self.rect.left < 0 and d_y + self.rect.top < 0) or \
           (d_x + self.rect.left < 0 and d_y + self.rect.bottom > 800) or \
           (d_x + self.rect.right > 1440 and d_y + self.rect.bottom > 800) or \
           (d_x + self.rect.right > 1440 and d_y + self.rect.top < 0):
                self.rect.center = (
                    self.rect.center[0] - d_x,
                    self.rect.center[1] - d_y
                )
                self.border = True
        if d_x + self.rect.left < 0:
            self.rect.center = (
                self.rect.center[0] - d_x,
                self.rect.center[1] + d_y
            )
            self.border = True
        elif d_x + self.rect.right > 1440:
            self.rect.center = (
                self.rect.center[0] - d_x,
                self.rect.center[1] + d_y
            )
            self.border = True
        elif d_y + self.rect.top < 0:
            self.rect.center = (
                self.rect.center[0] + d_x,
                self.rect.center[1] - d_y
            )
            self.border = True
        elif d_y + self.rect.bottom > 800:
            self.rect.center = (
                self.rect.center[0] + d_x,
                self.rect.center[1] - d_y
            )
            self.border = True
        elif self.state != 'flee' and self.state != 'hunt':
                self.rect.center = (
                    self.rect.center[0] + d_x,
                    self.rect.center[1] + d_y
                )
                self.border = False


    def update(self):
        if self.state == 'random':
            self.random_move()


class Predator(Agent):
    def __init__(self):
        super().__init__()
        self.image.fill('Orange')

    def chase_prey(self, closest_prey):
        position = Vector2(self.rect.center)
        if position.distance_to(closest_prey.rect.center) < self.sense:
            self.state = 'hunt'
            dirvect = Vector2(closest_prey.rect.center[0] - self.rect.center[0],
                              closest_prey.rect.center[1] - self.rect.center[1])
            dirvect.scale_to_length(self.speed)
            coords = [np.ceil(dv) if dv >= 0 else np.floor(dv) for dv in dirvect]
            self.random_move(*coords)
            if not self.border:
                self.rect.move_ip(coords)
        else:
            self.state = 'random'

class Prey(Agent):
    def __init__(self):
        super().__init__()
        self.image.fill('Grey')

    def flee(self, closest_predator):
        position = Vector2(self.rect.center)
        if position.distance_to(closest_predator.rect.center) < self.sense:
            self.state = 'flee'
            dirvect = Vector2(closest_predator.rect.center[0] + self.rect.center[0],
                              closest_predator.rect.center[1] + self.rect.center[1])
            dirvect.scale_to_length(self.speed)
            coords = [np.ceil(dv) if dv >= 0 else np.floor(dv) for dv in dirvect]
            self.rect.move_ip(coords)
            self.random_move(*coords)
            if not self.border:
                self.rect.move_ip(coords)
        else:
            self.state = 'random'

    def collect_resources(self, closest_resource):
        position = Vector2(self.rect.center)
        if position.distance_to(closest_resource.rect.center) < self.sense and self.state != 'flee':
            self.state = 'collecting'
            dirvect = Vector2(closest_resource.rect.center[0] - self.rect.center[0],
                              closest_resource.rect.center[1] - self.rect.center[1])
            dirvect.normalize()

            dirvect.scale_to_length(self.speed)
            coords = [np.ceil(dv) if dv >= 0 else np.floor(dv) for dv in dirvect]
            self.rect.move_ip(coords)
        else:
            self.state = 'random'


class Resources(Base):
    def __init__(self):
        super().__init__()
        self.image.fill('Green')